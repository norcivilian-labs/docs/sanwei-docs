# sanwei-docs

Documentation for [sanwei](https://gitlab.com/norcivilian-labs/sanwei), built with [mdBook](https://github.com/rust-lang/mdBook) and hosted at [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).
