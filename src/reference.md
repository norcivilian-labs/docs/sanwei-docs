# Reference

 - "--binary", required, path to Blender binary executable, e.g. "/Applications/Blender.app/Contents/MacOS/Blender"
 
 - "--font-path", required, path to chinese font, e.g. "./path/to/chinese.ttc"
 
 - "--font-name", required, name of the font in bpy.data.fonts, e.g. "Chinese Font Regular"
 
 - "--output", optional, name for output file without the .png extension, defaults to "/tmp/sanwei" and creates a "/tmp/sanwei.png" file
 
 - "--input", optional, arbitrary text to render, defaults to a random chinese character
 
 - "--resolution", optional, percentage scale for render resolution, int in [1, 32767], default 100
 
 - "--samples", optional, number of samples for the Cycles render engine, default 128
