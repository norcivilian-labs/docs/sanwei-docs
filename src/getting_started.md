# Getting Started

If you have [Nix](https://nixos.org) installed, run
```sh
nix build git+https://github.com/fetsorn/sanwei#image
```

Otherwise, install Blender, a Chinese font, and run in the shell
```shell
pip install sanwei

sanwei --binary /path/to/blender \
       --font-path "/path/to/font" \
       --font-name "Font Name Regular"
```

For a complete list of cli flags, see [Reference](./reference.md). Learn more about sanwei in the [Tutorial](./tutorial.md) and [User Guides](./user_guides.md).
